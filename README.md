Oblig 1 INF1510 - Stian Kongsvik

# Processing-kontrollpanel
###Oppgaven:
> Lage et kontrollpanel i processing. Kontrollpanelet skal ha minst 2 slider-e og 2 knapper. Slider-ne skal styre lysstyrken på 2 lysdioder, og knappene skal slå dem av og på.> Ønsker du en ekstra utfordring kan du også legge til slider-e for å styre tonen fra en piezoelement og vinkelen på en servomotor.

Jeg har valgt å gjøre denne oppgaven med piezoelement og servomotor.
I processing skal jeg skrive et program som lager et kontrollpanel med fire slidere og fire knapper, som hver skrur av/på to lysdioder, ett piezoelement og en servomotor. Sliderene vil justere verdiene som sendes til de fire elementene. Oppgaven spesifiserer ikke dette, men jeg har valgt i tillegg å bruke knapper som kan slå av og på piezo-elementet, og knapp på servoen vil sette den til null-posisjon.

For å skille signalene i seriellkommunikasjonen, vil jeg sende parvis byte der den første indikerer hva som skal kontrolleres (led, servo, … ), og den andre inneholder verdi fra 0 til 255. Jeg har valgt å bruke [controlP5](http://www.sojamo.de/libraries/controlP5/) for sliderene, men har skrevet kode for knappene selv.

### Utstyr:
- Arduino Uno + breadboard
- 2x led (en rød, en grønn)
- 2x 220Ω motstander
- 1x piezoelement (høyttaler)
- 1x Servomotor
- 1x 100µF kondensator

<div style="page-break-after: always;"></div>


## Kretsskjema
![image](http://cl.ly/image/3o1e1b381C26/schematic.png =500x)

<div style="page-break-after: always;"></div>

## Arduino og breadboard
![image](http://cl.ly/image/2G272j1V1v0R/arduino.png =500x)

<div style="page-break-after: always;"></div>

## Ferdig resultat

Det ferdige produktet fungerer som planlagt. De fire knappene på panelet skrur av og på komponentene, og sliderene justerer verdiene. Har ikke funnet unntak på utiltenkt bruk.

### Bilde av det ferdige produktet:
![image](http://cl.ly/image/3c10083r0A31/12905725944_ede11131c4_o.jpg =500x)
### Skjermbilde av kontrollpanelet 
![image](http://cl.ly/image/1S1A1T0a3e3H/Screenshot%202014-03-03%2014.09.57.png =500x)

## [Video av oppgaven](http://vimeo.com/88061849)