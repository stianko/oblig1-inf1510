/*
 * Oblig 1 INF1510
 * Stian Kongsvik
 * stiako
 *
 * Kontrollpanel i Processing
 *
 * Kode for Processing
 *
*/

// Importerer controlP5 for GUI-elementer (slidere) og serial for kommunikasjon
import controlP5.*;
import processing.serial.*;
PFont f;
Serial port;
int portnr = 2;
Button bRed, bGreen, bYellow,bBlue;
// Setter opp diverse variable og objekter
color red = #ED5F5A;
color redbright = #ED8480;
color green = #89D69B;
color greenbright = #A5D9B1;
color blue = #00BBFF;
color bluebright = #33C9FF;
color yellow = #F2E685;
color yellowbright = #F2EBAE;
ControlP5 cp5Red,cp5Green,cp5Yellow,cp5Blue,cp5Text;
int redB,greenB,yellowB,blueB;

// Setter opp knappene og vindu
void setup(){
  size(600, 400, "processing.core.PGraphicsRetina2D"); // Ekstra argument for retina-grafikk paa mac
  rectMode(RADIUS);
  //Oppretter knapper, setter posisjon og hvilken komponent som skal justeres, representert ved en char-verdi
  bRed = new Button(width/5.0,height * 3.0/4.0,'H',red);
  bGreen = new Button((width*2.0/5.0),(height * 3.0/4),'M',green);
  bYellow = new Button((width * 3.0)/5.0,height * 3.0/4.0,'S',yellow);
  bBlue = new Button((width * 4.0)/5.0,height * 3.0/4.0,'P',blue);
  
  //Oppretter slidere
  cp5Red = new ControlP5(this);
  cp5Red.addSlider("redB")
     .setPosition((width/5.0) - 20,height*2.0/5.0)
     .setColorForeground(red)
     .setColorActive(redbright)
     .setSize(40,100)
     .setRange(0,255)
     .setValue(128);
     
  cp5Green = new ControlP5(this);
  cp5Green.addSlider("greenB")
     .setPosition((width * 2.0/5.0) - 20,height*2.0/5.0)
     .setColorForeground(green)
     .setColorActive(greenbright)
     .setSize(40,100)
     .setRange(0,255)
     .setValue(128);
     
  cp5Yellow = new ControlP5(this);
  cp5Yellow.addSlider("yellowB")
     .setPosition((width * 3.0/5.0) - 20,height*2.0/5.0)
     .setColorForeground(yellow)
     .setColorActive(yellowbright)
     .setSize(40,100)
     .setRange(0,255)
     .setValue(128);
     
  cp5Blue = new ControlP5(this);
  cp5Blue.addSlider("blueB")
     .setPosition((width * 4.0/5.0) - 20,height*2.0/5.0)
     .setColorForeground(blue)
     .setColorActive(bluebright)
     .setSize(40,100)
     .setRange(0,255)
     .setValue(128);
     


  f = createFont("Inconsolata",14,true);
  println(Serial.list()); 
 
  //Oppretter seriellkommunikasjon
  port = new Serial(this, Serial.list()[portnr], 9600);
 
}

//Tegner knappene 
void draw(){
  background(255);
  textFont(f);
  fill(0);
  bRed.draw();
  bRed.setValue(byte(redB & (0xff)));
  bGreen.draw();
  bGreen.setValue(byte(greenB & (0xff)));
  bYellow.draw();
  bYellow.setValue(byte(yellowB & (0xff)));
  bBlue.draw();
  bBlue.setValue(byte(blueB & (0xff)));
 }
 
 //Sjekker om mus er klikket
 void mouseClicked(){
   bRed.mouseStatus();
   bGreen.mouseStatus();
   bYellow.mouseStatus();
   bBlue.mouseStatus();
 }
 
class Button{
  float boxX;
  float boxY;
  int boxSize = 20;
  boolean mouseOverBox = false;
  boolean mouseClicked = false;
  boolean status = false;
  char led;
  color c;
  byte out[] = new byte[2];
  
  //Konstruktoer for knapper, tar inn x/y-posisjon, hva den skal kontrollere og farge
  Button(float x, float y, char led, color c){
    this.boxX = x;
    this.boxY = y;
    this.led = led;
    this.c = c;
  }
  
  //Tegner knapper
  void draw(){
    if (mouseX > boxX-boxSize && mouseX < boxX+boxSize &&
        mouseY > boxY-boxSize && mouseY < boxY+boxSize) {
      mouseOverBox = true;
      stroke(0);
      fill(c);
    } else {
      // return the box to it's inactive state:
      if(status){
        stroke(0);
      } else {
        stroke(c);
      }
      fill(c);
      mouseOverBox = false;
    }
    rect(boxX, boxY, boxSize, boxSize);
  }
  
  //Metode som sjekker om knapp er trykket
  boolean getStatus(){
    return status;
  }
  
  //Metoden som sender verdiene over seriellkommunikasjon
  void setValue(byte v){
    //Hvis knapp er trykket inn, sender den hvilken verdi det gjelder og verdien av slideren
    if(status){
      out[0] = byte(led);
      out[1] = byte(v);
      port.write(out);
    } else { //Hvis knapp ikke er trykket, sender den 0
      out[0] = byte(led);
      out[1] = byte(0);
      port.write(out);
    }
  }
  
  //Sjekker om knapp blir trykket paa
  void mouseStatus(){
    mouseClicked = true;
    if(mouseOverBox && mouseClicked){
      if(!status){
        status = true;
      } else {
        status = false;
      }
    }
  }
}
