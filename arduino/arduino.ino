/*
 * Oblig 1 INF1510
 * Stian Kongsvik
 * stiako
 *
 * Kontrollpanel i Processing
 *
 * Kode for Arduino
 *
*/

// Importerer Servo-bibliotek
#include <Servo.h>
// Setter opp ny servo
Servo myServo;
// Setter diverse variable som brukes
const int ledPin1 = 6;
const int ledPin2 = 5;
const int piezoPin = 3;
const int servoPin = 9;
int angle;
int pitch;
int incomingLed;
byte incomingByte;
//Array for aa ta imot en byte for funksjon, og en for verdi
byte values[] = {0,0};
byte currentValue = 0;

void setup(){
  myServo.attach(9);
  Serial.begin(9600);
  pinMode(ledPin1,OUTPUT);
  pinMode(ledPin2,OUTPUT);
  pinMode(piezoPin,OUTPUT);
}

void loop() {
  // Sjekker om ny innkommende data
  if (Serial.available() > 0) {
    // Leser eldste byte
    incomingByte = Serial.read();
    
    //Lagrer siste byte i ett array der index 0 er byte for aa bestemme
    // hvilken utgang som skal styres, index 1 er verdien som utgangen skal gi
    values[currentValue] = incomingByte;
    currentValue++;
    if(currentValue > 1){
      currentValue = 0;
      // Roed led
      if(values[0] == 'H'){
        analogWrite(ledPin1, values[1]);
      }
      // Groenn led
      if(values[0] == 'M'){
        analogWrite(ledPin2, values[1]);
      }
      // Piezoelement
      if(values[0] == 'P'){
        if(values[1] != 0){
          // Mapper byte-verdien inn til mellom 50 og 4000 Hz
          pitch = map(int(values[1]), 0, 255, 50, 4000);
          tone(piezoPin, pitch);
        } else {
          noTone(piezoPin);
        }
      }
      // Servomotor
      if(values[0] == 'S'){
        // Mapper byte-verdien inn til vinkel mellom 0 og 180 grader
        angle = map(int(values[1]), 0 , 256, 0, 179);
        myServo.write(angle);
        delay(15);
      }
    }
  }
}
